﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 m_Movement;      //2  Make a Vector
    Animator m_Animator;     //6   Create a variable to store a reference to the Animator component,Get a reference to the Animator component

    public float turnSpeed= 20f;   //9 Create a turnSpeed variable       11:20f   Adjust the turnSpeed variable

    Quaternion m_Rotation = Quaternion.identity; //12  Create and Store a Rotation

    Rigidbody m_Rigidbody;//14 reference to the Rigidbody component

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();     //7    Set up the reference to the Animator component
        m_Rigidbody = GetComponent<Rigidbody>();    //15   setting the reference of the Animator variable
    }

    // Update is called once per frame
    void FixedUpdate()   //19 Change your Update Method
    {
        float horizontal = Input.GetAxis("Horizontal");    //1
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);    //3     Set the Values for your Variable
        m_Movement.Normalize();  //歸一化，大小改為一     //4   Normalizing a vector means keeping the vector’s direction the same, but changing its magnitude to 1

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);    //5   Identify Whether There is Player Input
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        m_Animator.SetBool("IsWalking", isWalking);   //8    Set the isWalking Animator Parameter

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);   //10 Calculate your character’s forward vector

        m_Rotation = Quaternion.LookRotation(desiredForward);  //13 This line simply calls the LookRotation method and creates a rotation looking in the direction of the given parameter.  
    }

    void OnAnimatorMove()    //16 This method allows you to apply root motion as you want, which means that movement and rotation can be applied separately.
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);  //17 movement
        m_Rigidbody.MoveRotation(m_Rotation);   //18 Rotation
    }

}
